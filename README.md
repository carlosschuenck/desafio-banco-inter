## Configuração

Aplicação desenvolvida com as seguintes versões:

- Apache Maven: 3.6.3
- JDK: 1.8.0_241
- Spring Boot: 2.3.0.RELEASE
- Banco de dados: H2

## 1.	Como	compilar	e	executar	a	sua	aplicação.

Com o CMD aberto na raiz no projeto, execute uma das seguintes opções: 
 
- mvn spring-boot:run
- ou
- mvn install -DskipTest && cd target && java -jar banco-inter-desafio.jar

Acesso:
- A aplicação irá ser disponibilizada na porta 8585.
- O endereço do Swagger: http://localhost:8585/swagger-ui.html

## 2.	Como	executar	os	testes	unitários.

Com o CMD aberto na raiz no projeto, execute: mvn test

## 3.	Sinta-se	livre	para	adicionar	qualquer	comentário.

- Banco de dados: O banco de dados será gerado automaticamente quando a aplicação for posta no ar.
- APIs: Foi mantido o nível 2 de maturidade. Pensei vocês pudessem achar desnecessário eu utilizar HATEOAS no desafio, se pensei errado, desculpe.
- Postman: A collection foi criada sem variáveis de ambiente para evitar o trabalho de aponta-las a quem fosse testar.
- Criptografia: As chaves publica e privada estão sendo trafegadas em base64.
- Teste de Integração: Não consegui pensar a tempo uma boa maneira de testar o Cache via integração, apenas nos testes unitários.
- Testes Unitário: Também não consegui pensar a tempo uma boa forma de testar a criptografia com Mock. Porém, foi feito teste de integração.
- Cache: Foi utilizada a estratégia LRU
- O código foi escrito em Português opcionalmente. Não tenho problemas em escrever em Inglês, se for o caso.
- Independente da avaliação. Se não for um incômodo, gostaria de receber ao menos um feedback técnico de melhoria, com o único objetivo de melhorar profissionalmente. Caso contrário, não tem problema.

