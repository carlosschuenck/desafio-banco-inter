package com.bancointer.desafio.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CaptadorErro extends ResponseEntityExceptionHandler {
 
    @ExceptionHandler(value = { ErroValidacao.class })
    protected ResponseEntity<Object> captadorErro(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException excecao, HttpHeaders headers, HttpStatus status, WebRequest request) {
           List<String> listaErro = excecao.getBindingResult()
        		   						   .getFieldErrors()
        		   						   .stream()
        		   						   .map(erro -> erro.getDefaultMessage())
        		   						   .collect(Collectors.toList());
           return new ResponseEntity<>(listaErro, status);
    }
    
    
}
