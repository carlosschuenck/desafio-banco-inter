package com.bancointer.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.NoArgsConstructor;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
@NoArgsConstructor
public class ErroValidacao extends RuntimeException {
	
	private static final long serialVersionUID = 6130397701151875789L;

	public ErroValidacao(String message) {
        super(message);
    }
}
