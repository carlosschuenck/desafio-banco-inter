package com.bancointer.desafio.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class DigitoUnico implements Serializable {
	
	private static final long serialVersionUID = -8451429998873069473L;
	
	@Id
	@GeneratedValue
	private Integer id;
	private String numero;
	private Integer numeroDeConcatenacoes;
	private Integer resultado;
	private Long usuarioId;
}
