package com.bancointer.desafio.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Criptografia implements Serializable {
	
	private static final long serialVersionUID = 4110003308582720618L;
	
	@Id
	private String emailUsuario;
	@Column(length = 1000)
	private String chavePublica;
	
}
