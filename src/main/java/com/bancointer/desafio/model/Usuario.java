package com.bancointer.desafio.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Usuario implements Serializable{
	
	private static final long serialVersionUID = 1617485852004713512L;
	
	@Id
	@GeneratedValue
	private Long id;
	@Column(length = 1000)
	private String nome;
	@Column(length = 1000)
	private String email;
	@OneToMany(mappedBy = "usuarioId", cascade = CascadeType.REMOVE)
	private Set<DigitoUnico> listaDigitoUnico;
}
