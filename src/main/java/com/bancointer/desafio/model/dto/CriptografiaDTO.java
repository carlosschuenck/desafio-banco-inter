package com.bancointer.desafio.model.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CriptografiaDTO implements Serializable {
	
	private static final long serialVersionUID = 1339591412141859473L;
	
	private Long id;
	@NotBlank(message = "Chave publica não informada")
	private String chavePublica;
	@NotBlank(message = "Email do usuário não informado")
	private String emailUsuario;
}
