package com.bancointer.desafio.model.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDTO implements Serializable {
	
	private static final long serialVersionUID = 7095431844575486971L;
	
	private Long id;
	@NotBlank(message = "Nome não informado")
	private String nome;
	@NotBlank(message = "Email não informado")
	private String email;
}
