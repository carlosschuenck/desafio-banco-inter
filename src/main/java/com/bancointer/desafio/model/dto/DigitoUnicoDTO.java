package com.bancointer.desafio.model.dto;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DigitoUnicoDTO implements Serializable {
	
	private static final long serialVersionUID = 1540416059972753716L;
	private static final String NUMERO_INVALIDO = "Numero inválido";
	
	private Long usuarioId;
	@NotBlank(message = "Numero não informado")
	private String numero;
	@NotNull(message = "Numero de concatenações não informado")
	@Min(value = 1, message = NUMERO_INVALIDO)
	@Max(value = 100000, message = NUMERO_INVALIDO)
	private Integer numeroDeConcatenacoes;

}
