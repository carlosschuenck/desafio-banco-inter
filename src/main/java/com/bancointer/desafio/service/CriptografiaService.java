package com.bancointer.desafio.service;

import static java.util.Base64.getDecoder;
import static java.util.Base64.getEncoder;
import static org.apache.commons.lang3.StringUtils.isAnyBlank;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancointer.desafio.exception.CaptadorErro;
import com.bancointer.desafio.exception.ErroValidacao;
import com.bancointer.desafio.model.Criptografia;
import com.bancointer.desafio.repository.CriptografiaRepository;


@Service
public class CriptografiaService {
	
	@Autowired
	private Cipher cifra;
	@Autowired
	private KeyFactory fabricaChave;
	@Autowired
	private CriptografiaRepository repository;
	
	private PublicKey publicKey;
	
	public void salvar(Criptografia criptografia){
		repository.save(criptografia);
	}
	
	public String criptografar(String conteudo) throws IOException, GeneralSecurityException {
		cifra.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] conteudoCriptografado = utilizarCifra(conteudo.getBytes());
		return new String(getEncoder().encode(conteudoCriptografado));
	}
	
	public String descriptografar(String conteudo, String chavePrivada) throws IOException, GeneralSecurityException {
		if(isAnyBlank(conteudo, chavePrivada)) {
			return conteudo;
		}
		cifra.init(Cipher.DECRYPT_MODE, converterChavePrivada(chavePrivada));
		byte[] conteudoDescriptografado = utilizarCifra(getDecoder().decode(conteudo));
		return new String(conteudoDescriptografado);
	}
	
	public void carregarChavePublica(String emailUsuario) throws InvalidKeySpecException {
		Criptografia criptografia = verificarSeChaveExisteParaUsuario(emailUsuario);
		converterChavePublica(criptografia.getChavePublica());
	}

	public Criptografia verificarSeChaveExisteParaUsuario(String emailUsuario) {
		Criptografia criptografia = repository.findByEmailUsuario(emailUsuario);
		if(criptografia == null) {
			throw new ErroValidacao("Chave publica não encontrada. Informar a chave publica para este email antes de realizar esta operação.");
		}
		return criptografia;
	}
	
	private PrivateKey converterChavePrivada(String chavePrivada) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
	   String chave = tratarChave(chavePrivada);
	   PKCS8EncodedKeySpec chavePrivadaObjeto = new PKCS8EncodedKeySpec(getDecoder().decode(chave));
	   return fabricaChave.generatePrivate(chavePrivadaObjeto);
	}
	
	/**
	 * Foi utilizado try/catch para auxilar na captura do erro quando a chave é invalida.
	 * O {@link CaptadorErro} não captura o erro BadPaddingException, por algum motivo ainda desconhecido.
	 * Não foi a melhor solução, mas foi a decisão tomada para entregar o teste um pouco mais completo e a tempo.
	 */
	public void converterChavePublica(String chavePublica) {
		String chave = tratarChave(chavePublica);
		X509EncodedKeySpec chavePublicaObjeto = new X509EncodedKeySpec(getDecoder().decode(chave));
		try {
			publicKey = fabricaChave.generatePublic(chavePublicaObjeto);
		} catch (InvalidKeySpecException e) {
			throw new ErroValidacao("Chave inválida");
		}
	}

	private String tratarChave(String chavePublica) {
		return chavePublica.toString().replaceAll("\\s+","");
	}
	
	/**
	 * Foi utilizado try/catch para auxilar na captura do erro quando a chave é invalida.
	 * O {@link CaptadorErro} não captura o erro BadPaddingException, por algum motivo ainda desconhecido.
	 * Não foi a melhor solução, mas foi a decisão tomada para entregar o teste um pouco mais completo e a tempo.
	 */
	private byte[] utilizarCifra(byte[] conteudo) throws IllegalBlockSizeException {
		try {
			return cifra.doFinal(conteudo);
		}  catch (BadPaddingException e) {
			throw new ErroValidacao("Chave inválida");
		}
	}
	
}