package com.bancointer.desafio.service;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bancointer.desafio.exception.ErroValidacao;
import com.bancointer.desafio.model.DigitoUnico;
import com.bancointer.desafio.repository.DigitoUnicoRepository;

@Service
public class DigitoUnicoService {
	
	@Autowired
	private DigitoUnicoRepository repository;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private CacheDigitoUnicoService cache;
	
	private static final String REGEX_VALIDAR_NUMERO = "[0-9]*";
	public int resultado;
	public BigInteger numero;
	public long numerosSeparadosSomados;
	public List<String> numerosSeparados;
	public StringBuilder builder;
	private DigitoUnico digitoUnico;
	
	@Transactional(readOnly = false)
	public Set<DigitoUnico> buscarPorUsuario(Long usuarioId){
		return repository.findByUsuarioId(usuarioId);
	}
	
	public void salvarSeUsuarioFoiInformado(DigitoUnico digitoUnico) {
		if(verficarSeUsuarioFoiInformado(digitoUnico.getUsuarioId())) {
			usuarioService.verificarSeUsuarioExiste(digitoUnico.getUsuarioId());
			repository.save(digitoUnico);
		}
	}
	
	/**
	 * Formula do cálculo: P = N * K
	 * @param numeroTexto - Referencia a letra N
	 * @param numeroDeConcatenacoes - Referencia a letra K
	 * @param usuarioId - Quem realizou o calculo. Não obrigatório 
	 * @return int - digitoUnico(P)
	 */
	public int calcularDigitoUnico(String numeroTexto, int numeroDeConcatenacoes, Long usuarioId) {
		validarParametros(numeroTexto, numeroDeConcatenacoes);
		concatenarNumeros(numeroTexto, numeroDeConcatenacoes);
		converterNumeroStringParaLong();
		descobrirDigitoUnico(numero);
		criarObjeto(numeroTexto, numeroDeConcatenacoes, usuarioId);
		salvarSeUsuarioFoiInformado(digitoUnico);
		cache.adicionar(digitoUnico);
		return resultado;
	}
	
	public void descobrirDigitoUnico(BigInteger numero) {
		if(verificarSeTemApenasUmDigito(numero)) {
			resultado = numero.intValue();
			return;
		}
		separarOsNumeros(numero);
		somarNumerosSeparados();
		descobrirDigitoUnico(BigInteger.valueOf(numerosSeparadosSomados));
	}

	public static void validarParametros(String numeroParaConcatenar, int numeroDeConcatenacoes) throws IllegalAccessError {
		validarNumero(numeroParaConcatenar);
		validarNumeroDeConcatenacoes(numeroDeConcatenacoes);
	}

	private void criarObjeto(String numeroParaConcatenar, int numeroDeConcatenacoes, Long usuarioId) {
		if(verficarSeUsuarioFoiInformado(usuarioId)) {
			usuarioService.verificarSeUsuarioExiste(usuarioId);
		}
		digitoUnico = DigitoUnico.builder()
								 .numero(numeroParaConcatenar)
								 .numeroDeConcatenacoes(numeroDeConcatenacoes)
								 .resultado(resultado)
								 .usuarioId(usuarioId)
								 .build();
	}
	
	private boolean verficarSeUsuarioFoiInformado(Long usuarioId) {
		return usuarioId != null;
	}
	
	private void converterNumeroStringParaLong() {
		numero = new BigInteger(builder.toString());
	}

	public void concatenarNumeros(String numero, int numeroDeConcatenacoes) {
		builder = new StringBuilder();
		for(int i = 0; i < numeroDeConcatenacoes; i++) {
			builder.append(numero);
		}
	}

	private static void validarNumeroDeConcatenacoes(int numeroDeConcatenacoes) {
		if(numeroDeConcatenacoes < 1 || numeroDeConcatenacoes > Math.pow(10,5)) {
			throw new ErroValidacao("O número de concatenações deve estar entre 1 e 10ˆ5");
		}
	}

	private static void validarNumero(String numero) throws IllegalAccessError {
		if(StringUtils.isBlank(numero) || naoEhNumero(numero) || parseLong(numero) < 1) {
			throw new ErroValidacao("Número incorreto!");
		}
	}

	private static boolean naoEhNumero(String numero) {
		return !numero.matches(REGEX_VALIDAR_NUMERO);
	}

	private void somarNumerosSeparados() {
		numerosSeparadosSomados = 0;
		numerosSeparados.forEach(numero -> numerosSeparadosSomados += parseInt(numero));
	}

	public void separarOsNumeros(BigInteger numero) {
		numerosSeparados = Arrays.asList(numero.toString().split(""));
	}
	
	public boolean verificarSeTemApenasUmDigito(BigInteger numero){
		return numero.toString().length() == 1;  
	}
}
