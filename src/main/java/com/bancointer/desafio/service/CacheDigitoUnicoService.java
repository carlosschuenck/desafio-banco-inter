package com.bancointer.desafio.service;

import static com.bancointer.desafio.service.DigitoUnicoService.validarParametros;

import java.util.Iterator;
import java.util.LinkedHashMap;

import com.bancointer.desafio.model.DigitoUnico;

/**
 * Estratégia utilizada: LRU
 */
public class CacheDigitoUnicoService {
	
	private int capacidadeMaxima;
    private static LinkedHashMap<String, DigitoUnico> map;
    
    public CacheDigitoUnicoService(int capacidadeMaxima) {
        this.capacidadeMaxima = capacidadeMaxima;
        map =  new LinkedHashMap<>();
    }
    
    public static String gerarChave(DigitoUnico digitoUnico) {
    	return gerarChave(digitoUnico.getNumero(), digitoUnico.getNumeroDeConcatenacoes());
    }
    
    public static String gerarChave(String numero, int numeroDeConcatenacoes) {
    	validarParametros(numero, numeroDeConcatenacoes);
    	return numero + numeroDeConcatenacoes;
    }
    
    public DigitoUnico buscar(String chave) {
        DigitoUnico value = map.get(chave);
        if (value == null) {
            return null;
        }
        adicionar(chave, value);
        return value;
    }

    public void adicionar(DigitoUnico digitoUnico) {
    	adicionar(gerarChave(digitoUnico), digitoUnico);
    }
    
    public void adicionar(String chave, DigitoUnico digitoUnico) {
        if (verificarSeExiste(chave)) {
            atualizarCache(chave, digitoUnico);
            return;
        } else if (verificarSeAtingiuCapacidadeMaxima()) {
            removerMaisAntigo();
        }
        adicionarNovo(chave, digitoUnico);
    }

    private boolean verificarSeExiste(String chave) {
    	return map.containsKey(chave);
    }
    
	private void adicionarNovo(String chave, DigitoUnico digitoUnico) {
		map.put(chave, digitoUnico);
	}

	private void atualizarCache(String chave, DigitoUnico digitoUnico) {
		map.remove(chave);
		adicionarNovo(chave, digitoUnico);
	}

	private void removerMaisAntigo() {
		Iterator<String> it = map.keySet().iterator();
		it.next();
		it.remove();
	}

	private boolean verificarSeAtingiuCapacidadeMaxima() {
		return map.size() == capacidadeMaxima;
	}
}
