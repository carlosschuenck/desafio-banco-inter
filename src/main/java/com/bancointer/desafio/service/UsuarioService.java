package com.bancointer.desafio.service;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.bancointer.desafio.exception.ErroValidacao;
import com.bancointer.desafio.model.Usuario;
import com.bancointer.desafio.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	@Autowired
	private CriptografiaService criptografiaService;
	
	public Usuario cadastrar(Usuario usuario) throws IOException, GeneralSecurityException {
		return repository.save(criptografarInformacoes(usuario));
	}

	public Usuario atualizar(Usuario usuario) throws IOException, GeneralSecurityException {
		verificarSeUsuarioExiste(usuario.getId());
		return repository.save(criptografarInformacoes(usuario));
	}
	
	public void deletar(Long id) {
		verificarSeUsuarioExiste(id);
		repository.deleteById(id);
	}
	
	public Usuario buscarPorId(Long id, String chavePrivada) throws IOException, GeneralSecurityException {
		Optional<Usuario> usuario = repository.findById(id);
		if(usuario.isPresent()) 
			return descriptografarInformacoes(usuario.get(), chavePrivada);
		throw new ResponseStatusException(NOT_FOUND, "Usuário não encontrado!");
	}
	
	private Usuario criptografarInformacoes(Usuario usuario) throws IOException, GeneralSecurityException {
		criptografiaService.carregarChavePublica(usuario.getEmail());
		return Usuario.builder()
					  .id(usuario.getId())
					  .email(criptografiaService.criptografar(usuario.getEmail()))
					  .nome(criptografiaService.criptografar(usuario.getNome()))
					  .build();
	}
	
	private Usuario descriptografarInformacoes(Usuario usuario, String chavePrivada) throws IOException, GeneralSecurityException {
		return Usuario.builder()
					  .id(usuario.getId())
					  .email(criptografiaService.descriptografar(usuario.getEmail(), chavePrivada))
					  .nome(criptografiaService.descriptografar(usuario.getNome(), chavePrivada))
					  .listaDigitoUnico(usuario.getListaDigitoUnico())
					  .build();
	}
	
	public void verificarSeUsuarioExiste(Long id) {
		if(id != null && repository.existsById(id)) {
			return;
		}
		throw new ErroValidacao("Usuário não existe!");
	}
}
