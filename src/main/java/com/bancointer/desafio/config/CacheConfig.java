package com.bancointer.desafio.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bancointer.desafio.service.CacheDigitoUnicoService;

@Configuration
public class CacheConfig {
	
	@Value("${cache.capacidade}")
	private int capacidade;
	
	@Bean
	public CacheDigitoUnicoService inicializarCacheDigitoUnico() {
		return new CacheDigitoUnicoService(capacidade);
	}
}
