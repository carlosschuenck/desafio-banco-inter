package com.bancointer.desafio.config;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CriptografiaConfig {
	
	private static final String TIPO_CRIPTOGRAFIA = "RSA";

	@Bean
	public Cipher instanciarChiper() throws NoSuchAlgorithmException, NoSuchPaddingException {
		return Cipher.getInstance(TIPO_CRIPTOGRAFIA);
	}
	
	@Bean
	public KeyFactory instanciarKeyFactory() throws IOException, GeneralSecurityException{
		return KeyFactory.getInstance(TIPO_CRIPTOGRAFIA);
	}
	
}
