package com.bancointer.desafio.resource;

import static com.bancointer.desafio.service.CacheDigitoUnicoService.gerarChave;
import static org.springframework.http.HttpStatus.OK;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bancointer.desafio.model.DigitoUnico;
import com.bancointer.desafio.model.dto.DigitoUnicoDTO;
import com.bancointer.desafio.service.CacheDigitoUnicoService;
import com.bancointer.desafio.service.DigitoUnicoService;


/**
 * Mantive o nível 2 de maturidade da API.
 */
@RestController
@RequestMapping("/digito-unico")
public class DigitoUnicoResource {
	
	@Autowired
	private DigitoUnicoService digitoUnicoService;
	
	@Autowired
	private CacheDigitoUnicoService cache;
	
	@PostMapping("/calcular")
	@ResponseStatus(OK)
	public Integer calcularDigitoUnico(@Valid @RequestBody DigitoUnicoDTO dto) {
		DigitoUnico digitoUnico = cache.buscar(gerarChave(dto.getNumero(), dto.getNumeroDeConcatenacoes()));
		if(digitoUnico == null) {
			return digitoUnicoService.calcularDigitoUnico(dto.getNumero(), dto.getNumeroDeConcatenacoes(), dto.getUsuarioId());
		}
		digitoUnico.setUsuarioId(dto.getUsuarioId());
		digitoUnicoService.salvarSeUsuarioFoiInformado(digitoUnico);
		return digitoUnico.getResultado();
	}

	
	@GetMapping("/usuario/{id}")
	@ResponseStatus(OK)
	public Set<DigitoUnico> buscarPorUsuario(@PathVariable("id") Long id){
		return digitoUnicoService.buscarPorUsuario(id);
	}
}
