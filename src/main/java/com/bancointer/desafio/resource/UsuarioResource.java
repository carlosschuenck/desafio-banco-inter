package com.bancointer.desafio.resource;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bancointer.desafio.model.Usuario;
import com.bancointer.desafio.model.dto.UsuarioDTO;
import com.bancointer.desafio.service.UsuarioService;

/**
 * Mantive o nível 2 de maturidade da API.
 */
@RestController
@RequestMapping("/usuario")
public class UsuarioResource {
	
	@Autowired
	private UsuarioService service;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	@ResponseStatus(CREATED)
	public Usuario cadastrar(@Valid @RequestBody UsuarioDTO usuario) throws IOException, GeneralSecurityException{
		return service.cadastrar(modelMapper.map(usuario, Usuario.class));
	}
	
	@PutMapping
	@ResponseStatus(OK)
	public Usuario atualizar(@Valid @RequestBody UsuarioDTO usuario) throws IOException, GeneralSecurityException{
		return service.atualizar(modelMapper.map(usuario, Usuario.class));
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(OK)
	public Usuario buscarPorId(@PathVariable("id") Long id, @RequestBody(required = false) String chavePrivada) throws IOException, GeneralSecurityException{
		return service.buscarPorId(id, chavePrivada);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(NO_CONTENT)
	public void deletar(@PathVariable("id") Long id){
		service.deletar(id);
	}
}
