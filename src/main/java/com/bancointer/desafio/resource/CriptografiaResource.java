package com.bancointer.desafio.resource;

import static org.springframework.http.HttpStatus.CREATED;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bancointer.desafio.model.Criptografia;
import com.bancointer.desafio.model.dto.CriptografiaDTO;
import com.bancointer.desafio.service.CriptografiaService;

/**
 * Mantive o nível 2 de maturidade da API.
 */
@RestController
@RequestMapping("/criptografia")
public class CriptografiaResource {
	
	@Autowired
	private CriptografiaService criptografiaService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping("/chave-publica")
	@ResponseStatus(CREATED)
	public void salvarChavePublica(@RequestBody CriptografiaDTO criptografiaDTO) {
		criptografiaService.salvar(modelMapper.map(criptografiaDTO, Criptografia.class));
	}
}
