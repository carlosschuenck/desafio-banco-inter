package com.bancointer.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bancointer.desafio.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {}
