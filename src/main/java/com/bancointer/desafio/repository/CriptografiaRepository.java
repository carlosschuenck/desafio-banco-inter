package com.bancointer.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bancointer.desafio.model.Criptografia;

public interface CriptografiaRepository extends JpaRepository<Criptografia, String>{
	Criptografia findByEmailUsuario(String emailUsuario);
}
