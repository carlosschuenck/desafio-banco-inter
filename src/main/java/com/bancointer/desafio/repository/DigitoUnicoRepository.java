package com.bancointer.desafio.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bancointer.desafio.model.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Integer> {
	Set<DigitoUnico> findByUsuarioId(Long usuarioId);
}
