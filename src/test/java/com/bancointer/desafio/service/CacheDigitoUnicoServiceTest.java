package com.bancointer.desafio.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bancointer.desafio.model.DigitoUnico;
import com.bancointer.desafio.service.CacheDigitoUnicoService;

@DisplayName("Testando CacheDigitoUnicoService")
public class CacheDigitoUnicoServiceTest {
	
	private static final String NUMERO_BASE = "9875";
	private CacheDigitoUnicoService cache;
	
	@BeforeEach
    public void instanciarCache() {
		// O cache foi testado com a capacidade 2 para facilitar a feitura e execução do mesmo.
        cache = new CacheDigitoUnicoService(2);
    }
    
	@DisplayName("Adicionar objeto no cache")
    @Test
    public void testAdicionarNoCache() {
    	DigitoUnico primeiroDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 1);
    	String primeiraChave = CacheDigitoUnicoService.gerarChave(primeiroDigitoUnico);
        cache.adicionar(primeiraChave, primeiroDigitoUnico);
        assertEquals(cache.buscar(primeiraChave), primeiroDigitoUnico);
    }
    
	@DisplayName("Gerar chave que identifica o objeto no cache")
    @Test
    public void testGerarChave() {
    	int numeroDeConcatenacoes = 1;
    	DigitoUnico primeiroDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, numeroDeConcatenacoes);
    	String chaveEsperada = NUMERO_BASE + numeroDeConcatenacoes;
        assertEquals(CacheDigitoUnicoService.gerarChave(primeiroDigitoUnico), chaveEsperada);
        assertEquals(CacheDigitoUnicoService.gerarChave(NUMERO_BASE, 1), chaveEsperada);
    }
    
	@DisplayName("Verificar a capacidade máxima e remover o menos utilizado.")
    @Test
    public void testCapacidadeMaxima() {
    	DigitoUnico primeiroDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 1);
    	String primeiraChave = CacheDigitoUnicoService.gerarChave(primeiroDigitoUnico);
        cache.adicionar(primeiraChave, primeiroDigitoUnico);
        assertEquals(cache.buscar(primeiraChave), primeiroDigitoUnico);
        
    	DigitoUnico segundoDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 4);
    	String segundaChave = CacheDigitoUnicoService.gerarChave(segundoDigitoUnico);
        cache.adicionar(segundaChave, segundoDigitoUnico);
        assertEquals(cache.buscar(primeiraChave), primeiroDigitoUnico);
        assertEquals(cache.buscar(segundaChave), segundoDigitoUnico);
        
        DigitoUnico terceiroDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 3);
    	String terceiraChave = CacheDigitoUnicoService.gerarChave(terceiroDigitoUnico);
    	cache.adicionar(terceiraChave, terceiroDigitoUnico);
    	assertEquals(cache.buscar(primeiraChave), null);
    	assertEquals(cache.buscar(segundaChave), segundoDigitoUnico);
    	assertEquals(cache.buscar(terceiraChave), terceiroDigitoUnico);
    }
	
	@DisplayName("Evitar duplicidade de objeto no cache")
    @Test
    public void testEvitarDuplicidade() {
    	DigitoUnico primeiroDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 1);
    	String primeiraChave = CacheDigitoUnicoService.gerarChave(primeiroDigitoUnico);
    	
    	DigitoUnico segundoDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 2);
    	String segundaChave = CacheDigitoUnicoService.gerarChave(segundoDigitoUnico);
        cache.adicionar(segundaChave, segundoDigitoUnico);
        cache.adicionar(primeiraChave, primeiroDigitoUnico);
        cache.adicionar(primeiraChave, primeiroDigitoUnico);
        cache.adicionar(primeiraChave, primeiroDigitoUnico);
        assertEquals(cache.buscar(primeiraChave), primeiroDigitoUnico);
        assertEquals(cache.buscar(segundaChave), segundoDigitoUnico);
    }
	
	@DisplayName("Renovar objeto no cache")
    @Test
    public void testRenovarDigitoUnicoNoCache() {
    	DigitoUnico primeiroDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 1);
    	String primeiraChave = CacheDigitoUnicoService.gerarChave(primeiroDigitoUnico);
    	
    	DigitoUnico segundoDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 2);
    	String segundaChave = CacheDigitoUnicoService.gerarChave(segundoDigitoUnico);
    	
    	cache.adicionar(primeiraChave, primeiroDigitoUnico);
    	cache.adicionar(segundaChave, segundoDigitoUnico);
        assertEquals(cache.buscar(primeiraChave), primeiroDigitoUnico);
        
        DigitoUnico terceiroDigitoUnico = criarObjetoDigitoUnicoFalso(NUMERO_BASE, 3);
    	String terceiraChave = CacheDigitoUnicoService.gerarChave(terceiroDigitoUnico);
        cache.adicionar(terceiraChave, terceiroDigitoUnico);
        
        assertEquals(cache.buscar(primeiraChave), primeiroDigitoUnico);
        assertEquals(cache.buscar(segundaChave), null);
        assertEquals(cache.buscar(terceiraChave), terceiroDigitoUnico);
    }
    
    private DigitoUnico criarObjetoDigitoUnicoFalso(String numero, int numeroDeConcatenacoes) {
    	return  DigitoUnico.builder()
			    			.numero(numero)
			    			.numeroDeConcatenacoes(numeroDeConcatenacoes)
			    			.resultado(123456)
			    			.build();
    }
}
