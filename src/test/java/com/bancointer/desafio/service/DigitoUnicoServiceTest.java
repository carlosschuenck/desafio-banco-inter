package com.bancointer.desafio.service;

import static java.math.BigInteger.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.bancointer.desafio.exception.ErroValidacao;
import com.bancointer.desafio.repository.DigitoUnicoRepository;

@DisplayName("Testando DigitoUnicoService")
@ExtendWith(MockitoExtension.class)
public class DigitoUnicoServiceTest {
	
	private static final String NUMERO_BASE_PARA_CALCULAR = "9875";
	private static final int NUMERO_DE_CONCATENACOES = 4;
	
	@Mock
	private DigitoUnicoRepository repository;
	@Mock
	private UsuarioService usuarioService;
	@Mock
	private CacheDigitoUnicoService cache;
	
	@Autowired
	@InjectMocks
	DigitoUnicoService digitoUnicoService;
	
	@DisplayName("Realizar calculo do digito único do número 9875 concatenado 4x")
	@Test
	void testDigitoUnico(){
		assertEquals(8, digitoUnicoService.calcularDigitoUnico(NUMERO_BASE_PARA_CALCULAR, NUMERO_DE_CONCATENACOES, 1L));
	}
	
	@DisplayName("Realizar calculo do digito único do número 9875")
	@Test
	void testCalcularDigitoUnico(){
		digitoUnicoService.descobrirDigitoUnico(valueOf(9875));
		assertEquals(2, digitoUnicoService.resultado);
	}
	
	@DisplayName("Verificar se o número tem apenas um dígito ou mais")
	@Test
	void testVerificarSeTemApenasUmDigito() {
		assertTrue(digitoUnicoService.verificarSeTemApenasUmDigito(valueOf(1)));
		assertFalse(digitoUnicoService.verificarSeTemApenasUmDigito(valueOf(99)));
	}
	
	@DisplayName("Separar números")
	@Test
	void testSepararOsNumeros() {
		digitoUnicoService.separarOsNumeros(valueOf(123L));
		assertTrue(digitoUnicoService.numerosSeparados.size() == 3);
		assertTrue(digitoUnicoService.numerosSeparados.get(0).equals("1"));
		assertTrue(digitoUnicoService.numerosSeparados.get(1).equals("2"));
		assertTrue(digitoUnicoService.numerosSeparados.get(2).equals("3"));
	}
	
	@DisplayName("Tentar calcular com parametros inválidos")
	@Test
	void testDigitoUnicoComParametroInvalido(){
		assertThrows(ErroValidacao.class, () -> digitoUnicoService.calcularDigitoUnico(null, NUMERO_DE_CONCATENACOES, null));
		assertThrows(ErroValidacao.class, () -> digitoUnicoService.calcularDigitoUnico("-15", NUMERO_DE_CONCATENACOES, null));
		assertThrows(ErroValidacao.class, () -> digitoUnicoService.calcularDigitoUnico("0", NUMERO_DE_CONCATENACOES, null));
		assertThrows(ErroValidacao.class, () -> digitoUnicoService.calcularDigitoUnico("abc", NUMERO_DE_CONCATENACOES, null));
		assertThrows(ErroValidacao.class, () -> digitoUnicoService.calcularDigitoUnico("", NUMERO_DE_CONCATENACOES, null));
		assertThrows(ErroValidacao.class, () -> digitoUnicoService.calcularDigitoUnico(NUMERO_BASE_PARA_CALCULAR, 0, null));
		assertThrows(ErroValidacao.class, () -> digitoUnicoService.calcularDigitoUnico(NUMERO_BASE_PARA_CALCULAR, 100001, null));
	}
	
	@DisplayName("Concatenar de acordo com o número de vezes")
	@Test
	void testConcatenarNumeros() {
		digitoUnicoService.concatenarNumeros(NUMERO_BASE_PARA_CALCULAR, NUMERO_DE_CONCATENACOES);
		String resultadoEsperado = "9875987598759875";
		assertEquals(resultadoEsperado, digitoUnicoService.builder.toString());
	}
	
}
