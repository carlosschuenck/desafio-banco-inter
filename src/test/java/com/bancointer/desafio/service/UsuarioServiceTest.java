package com.bancointer.desafio.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import com.bancointer.desafio.exception.ErroValidacao;
import com.bancointer.desafio.repository.UsuarioRepository;

@DisplayName("Testando UsuarioService")
@ExtendWith(MockitoExtension.class)
public class UsuarioServiceTest {
	
	@Mock
	private UsuarioRepository repository;
	
	@Mock
	private CriptografiaService criptografiaService;
	
	@Autowired
	@InjectMocks
	private UsuarioService usuarioService;
	
	@DisplayName("Verificar se o usuário informado existe ou não")
	@Test
	void testVerificarSeUsuarioExiste() {
		when(repository.existsById(1L)).thenReturn(false);
		assertThrows(ErroValidacao.class, () -> usuarioService.verificarSeUsuarioExiste(1L));
		assertThrows(ErroValidacao.class, () -> usuarioService.verificarSeUsuarioExiste(null));
		when(repository.existsById(2L)).thenReturn(true);
		usuarioService.verificarSeUsuarioExiste(2L);
	}
	
	@DisplayName("Buscar utilizando id inexistente")
	@Test
	void testBuscarPorIdInexistente() {
		when(repository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(ResponseStatusException.class, () -> usuarioService.buscarPorId(1L, ""));
	}
}
