FROM maven:3.6.3-jdk-8 as desenvolvimento
ENV BASE_PATH /usr/desafio
WORKDIR $BASE_PATH
COPY . .
EXPOSE 8585
CMD [ "mvn", "spring-boot:run" ]

FROM desenvolvimento as teste
WORKDIR $BASE_PATH
COPY --from=desenvolvimento $BASE_PATH .
CMD ["mvn", "test"]

FROM desenvolvimento as build
WORKDIR $BASE_PATH
COPY --from=desenvolvimento $BASE_PATH .
RUN mvn install -DskipTest

FROM openjdk:8-jre-alpine as producao
COPY --from=build /usr/desafio/target/banco-inter-desafio.jar .
EXPOSE 8585
CMD ["java", "-jar", "banco-inter-desafio.jar"]